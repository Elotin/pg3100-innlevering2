/*
 * Innlevering 2
 * Håkon Martin Eide.2.klasse programmering 2012
 * 22,10.2012
 */

package main;

public class AccountInterestUpdater extends AccountUpdater {
    private String accountNumber;
    private Actions action;
    private double value;

    protected AccountInterestUpdater(String accountNumber, Actions action, double value) {
        super();
        this.accountNumber = accountNumber;
        this.action = action;
        this.value = value;
    }

    @Override
    public Account executeUpdate(String tableName) {
        Account accountToUpdate = Account.GetAccount(accountNumber, tableName);
        if (accountToUpdate != null){
            double newInterest = accountToUpdate.getAccountInterest();
            if (action == Actions.ADD)
                newInterest += value;
            else if (action == Actions.SUBTRACT)
                newInterest -= value;
            else if (action == Actions.REPLACE)
                newInterest = value;

            return Account.UpdateAccount(accountNumber, accountToUpdate.getAccountBalance(), newInterest, tableName);
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return String.format("Account Number: %s, account value: %d", accountNumber, value);
    }
}
