/*
 * Innlevering 2
 * Håkon Martin Eide.2.klasse programmering 2012
 * 22,10.2012
 */

package main;


class AccountBalanceUpdater extends AccountUpdater {
    private String accountNumber;
    private Actions action;
    private double value;

    protected AccountBalanceUpdater(String accountNumber, AccountUpdater.Actions action, double value) {
        super();
        this.accountNumber = accountNumber;
        this.action = action;
        this.value = value;
    }

    @Override
    public Account executeUpdate(String tableName) {
        Account accountToUpdate = Account.GetAccount(accountNumber, tableName);
        if (accountToUpdate != null){
            double newBalance = accountToUpdate.getAccountBalance();
            if (action == Actions.ADD)
                newBalance += value;
            else if (action == Actions.SUBTRACT)
                newBalance -= value;
            else if (action == Actions.REPLACE)
                newBalance = value;

            return Account.UpdateAccount(accountNumber, newBalance, accountToUpdate.getAccountInterest(), tableName);
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return String.format("Account Number: %s, account value: %d", accountNumber, value);
    }
}
