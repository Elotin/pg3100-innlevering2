/*
 * Innlevering 2
 * Håkon Martin Eide.2.klasse programmering 2012
 * 22,10.2012
 */

package main;

public class AccountNewEntryUpdater extends AccountUpdater {
    private double balance;
    private double interest;
    private String accountNumber;


    protected AccountNewEntryUpdater(String accountNumber, double balance, double interest) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.interest = interest;
    }

    @Override
    public Account executeUpdate(String tableName) {
        if (Account.GetAccount(accountNumber, tableName) == null )
            return Account.CreateAccount(accountNumber, balance, interest, tableName);
        else
            return null;
    }

    public String toString() {
        return String.format("Account Number: %s, account value: %d", accountNumber, balance);
    }
}
