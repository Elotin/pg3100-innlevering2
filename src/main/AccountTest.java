/*
 * Innlevering 2
 * Håkon Martin Eide.2.klasse programmering 2012
 * 22,10.2012
 */

package main;

import org.junit.After;
import org.junit.Test;

import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.*;

public class AccountTest {

	private String tableName = "accounts";
	private String acccountNumber = "12345678";


	@After
	public void tearDown() throws Exception {
		Connection connection = new DBConnection("JavaDevUser", "JavaDev").getDatabaseConnection();
		try {
			Statement stmnt = connection.createStatement();
			stmnt.executeUpdate("delete from " + tableName + " where accountNumber = " + acccountNumber);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	
	@Test
	public void CheckThatAccountIsCreatedProperly() throws SQLException {
		int accountBalance = 4000;
		double accountInterest = 2.5;
		
		
		Account newAccount = null;
		try {
			newAccount = Account.CreateAccount(acccountNumber, accountBalance, accountInterest, tableName);
		} catch (InvalidParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(newAccount.getAccountNumber(), acccountNumber);
		assertEquals(newAccount.getAccountBalance(), accountBalance, 0.001);
		assertEquals(newAccount.getAccountInterest(), accountInterest, 0.001);
	}
	@Test
	public void CheckThatInvalidAccountNumberReturnsFalse(){
			assertFalse("9 assertet true", Account.validateAccountNumber("9"));
			assertFalse("0 assertet true", Account.validateAccountNumber("0"));
			assertFalse("123456789 assertet true", Account.validateAccountNumber("123456789"));
			assertFalse("-123456789 assertet true", Account.validateAccountNumber("-12345678"));
			assertFalse("-123 assertet true", Account.validateAccountNumber("-123"));
			assertTrue("12345678 assertet false", Account.validateAccountNumber("12345678"));
	}
	
	
	@Test
	public void CheckThatGetAccountReturnsNullForUnfoundAccount(){
		Account getAccount = null;
        getAccount = Account.GetAccount(acccountNumber, tableName);
        assertNull(getAccount);
	}
	
	@Test
	public void CheckThatGetAccountReturnsValidAccountForValidEntry(){
		double accountBalance = 2000;
		double accountInterest = 3;
		String dummyAccountNumber = "87654321";
		
		Account getAccount = null;
        getAccount = Account.GetAccount(dummyAccountNumber, tableName);


        assertEquals(String.format("%s was returned, %s was expected", dummyAccountNumber, getAccount.getAccountNumber()),dummyAccountNumber, getAccount.getAccountNumber());
		assertEquals(accountBalance, getAccount.getAccountBalance(), 0.00001);
		assertEquals(accountInterest, getAccount.getAccountInterest(), 0.0001);
	}
	@Test
	public void CheckThatUpdateAccountUpdatesAccountFound() throws SQLException{
		String dummyAccountNumber = "12345678";
		double account1Balance = 5000;
		double account1Interest = 1.2;
		double account2Balance = 3000;
		double account2Interest = 1.6;
		
		Account newAccount = null;
		try {
			newAccount = Account.CreateAccount(acccountNumber, account1Balance, account2Balance, tableName);
		} catch (InvalidParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		newAccount = Account.UpdateAccount(dummyAccountNumber, account2Balance, account2Interest, tableName);
		
		assertEquals(dummyAccountNumber, newAccount.getAccountNumber());
		assertEquals(account2Balance, newAccount.getAccountBalance(), 0.00001);
		assertEquals(account2Interest, newAccount.getAccountInterest(), 0.0001);
		
		newAccount = Account.UpdateAccount(dummyAccountNumber, account1Balance, account1Interest, tableName);
		
		assertEquals(dummyAccountNumber, newAccount.getAccountNumber());
		assertEquals(account1Balance, newAccount.getAccountBalance(), 0.00001);
		assertEquals(account1Interest, newAccount.getAccountInterest(), 0.0001);
	}
	
	@Test
	public void CheckThatDeleteValidAccountReturnsTrue() throws InvalidParameterException, SQLException{
		String dummyAccountNumber = "12344321";
		double accountBalance = 2000;
		double interest = 25;
		
		Account newAccount = Account.CreateAccount(dummyAccountNumber, accountBalance, interest, tableName);
		
		assertTrue(Account.DeleteAccount(dummyAccountNumber, tableName));
	}
	
	@Test
	public void CheckThatDeleteNonExistantAccountReturnsFalse() throws SQLException{
		String dummyAccountNumber = "12344321";
		double accountBalance = 2000;
		double interest = 25;
		
		assertFalse(Account.DeleteAccount(dummyAccountNumber, tableName));
	}

    @Test
    public void CheckThatGetAllAccountsDoNotCrash(){
        assertNotNull(Account.GetAllAccounts(tableName));
    }

    @Test
    public void CheckThatGetAllAccountsReturnsMoreThanZero(){
        assertTrue(0 < Account.GetAllAccounts(tableName).size());
    }

}
