/*
 * Innlevering 2
 * Håkon Martin Eide.2.klasse programmering 2012
 * 22,10.2012
 */
package main;

import java.sql.*;

public class DBConnection {
	
	private String brukernavn = "JavaDevUser";
	private String passord = "JavaDev";
	//DatabaseURL is the base url to the database server, without the database name appended onto it.
	private String databaseURL = "jdbc:mysql://hmeide.com/JavaDev";
	private Connection databaseConnection;
	
	/*
	 * Default constructor. Will use the preassigned database for logging in.
	 * Will assume you always want to create a connection if you create an instance of this class
	 * and will therefore connect right away.
	 */
    public DBConnection(){
        ConnectToTheDatabase();
    }

	public DBConnection(String brukernavn, String passord)
	{
        this.brukernavn = brukernavn;
        this.passord = passord;
		ConnectToTheDatabase();
	}

    public Connection getDatabaseConnection() {
		return databaseConnection;
	}
	
	public void closeDatabaseConnection() 
	{
		try {
			databaseConnection.close();
			System.out.println("DB connection closed");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Failed to close database connection");
		}
	}
	private void ConnectToTheDatabase()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			System.out.println("com.mysql.jdbc.driver not found");
		}
		try {
			databaseConnection = DriverManager.getConnection(databaseURL, brukernavn, passord);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Connection failed");
		} catch (NullPointerException e)
		{
			System.out.println("null pointer");
		}
	}
}
