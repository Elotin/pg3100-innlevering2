/*
 * Innlevering 2
 * Håkon Martin Eide.2.klasse programmering 2012
 * 22,10.2012
 */

package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
The accountMaintenance defined in the task.
Uses alot of methods on the different classes to keep the implementation
in this class as small and easy to read as possible.
 */
public class AccountMaintenance {
    /*
    Gets all the accountUpdate objects from the tableUpdate and executes them on the tablename.
     */
    public static void updateAccounts(String tablename, String tableUpdate){
        ArrayList<AccountUpdater> accountUpdaters = AccountUpdater.getAllAccountUpdates(tableUpdate);
        for (AccountUpdater accountUpdater : accountUpdaters) {
            accountUpdater.executeUpdate(tablename);
        }
    }
    /*
    Gets all the accounts from tablename and maps them to a Map with accountNumber as the key
    and the Account as the value.
     */
    public static Map<String, Account> getAccounts(String tableName) {

        Map<String, Account> accountsMap = new HashMap<String, Account>();
        ArrayList<Account> accountsList = Account.GetAllAccounts(tableName);
        for (Account account : accountsList) {
            accountsMap.put(account.getAccountNumber(), account);
        }
        return accountsMap;
    }
    /*
    Returns the account for the given accountNumber in the tablename
     */
    public static Account getAccount(String tableName, String accountNumber) {
        return Account.GetAccount(accountNumber, tableName);
    }
}
