/*
 * Innlevering 2
 * Håkon Martin Eide.2.klasse programmering 2012
 * 22,10.2012
 */

package main;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

 class AccountMaintenanceTest {

    @Test
    public void CheckThatGetAccountReturnsNullForUnfoundAccount(){
        String acccountNumber = "12345678";
        String tableName = "accounts";

        Account getAccount = null;
        getAccount = AccountMaintenance.getAccount(tableName, acccountNumber);
        assertNull(getAccount);
    }

    @Test
    public void CheckThatGetAccountReturnsValidAccountForValidEntry(){
        double accountBalance = 2000;
        double accountInterest = 1.5;
        String dummyAccountNumber = "87654321";
        String tableName = "accounts";

        Account getAccount = null;
        getAccount = AccountMaintenance.getAccount(tableName, dummyAccountNumber);
        getAccount = Account.GetAccount("87654321", tableName);


        assertEquals(Integer.valueOf(dummyAccountNumber), Integer.valueOf(getAccount.getAccountNumber()));
        assertEquals(accountBalance, getAccount.getAccountBalance(), 0.00001);
        assertEquals(accountInterest, getAccount.getAccountInterest(), 0.0001);
    }
}
