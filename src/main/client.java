/*
 * Innlevering 2
 * Håkon Martin Eide.2.klasse programmering 2012
 * 22,10.2012
 */

package main;

import java.util.Map;

public class client {
    public static void main(String [] args) {
        Map<String, Account> accountMap =  AccountMaintenance.getAccounts("accounts");
        printAllMapEntries(accountMap);

        AccountMaintenance.updateAccounts("accounts", "accountUpdate");

        accountMap = AccountMaintenance.getAccounts("accounts");
        printAllMapEntries(accountMap);

        AccountMaintenance.getAccount("accounts", "12345678");

    }

    private static void printAllMapEntries(Map<String, Account> accountMap) {
        for (Map.Entry<String, Account> accountEntry : accountMap.entrySet()) {
            Map.Entry entry = (Map.Entry) accountEntry;
            System.out.println(entry.getKey() + "  " + entry.getValue().toString());
        }
    }
}
