/*
 * Innlevering 2
 * Håkon Martin Eide.2.klasse programmering 2012
 * 22,10.2012
 */

package main;

import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/*
The purpose of the account class is that all actions directly against the database are handled via static CRUD operations.
To get an account you give the class the accountnumber and table to find it in and it will return an instance of account with
the values of that account if it finds any.
 */
public class Account {

    private String accountNumber;
    private double accountBalance;
    private double accountInterest;

    private Account(String accountNumber, double accountBalance, double accountInterest) {
        this.setAccountBalance(accountBalance);
        this.setAccountNumber(accountNumber);
        this.setAccountInterest(accountInterest);
    }
    /*
    Takes all the values needed to create an account and what table to put it in, establishes a connection to the database, prepares a statement and executes it.
    In the end it returns an Account object with the values you put in.
     */
    public static Account CreateAccount(String accountNumber, double accountBalance, double accountInterest, String tableName) throws InvalidParameterException {
        if (!validateAccountNumber(accountNumber)) {
            throw new InvalidParameterException(String.format("%s is not a valid account number", accountNumber));
        }
        Connection connection = getNewConnection();

        PreparedStatement statement;
        try {
            statement = connection.prepareStatement("insert into " + tableName + " values(?, ?, ?)");
            statement.setString(1, accountNumber);
            statement.setDouble(2, accountBalance);
            statement.setDouble(3, accountInterest);

            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        return new Account(accountNumber, accountBalance, accountInterest);
    }
    /*
    Searches the Database table for a row with a matching account number, fetches the values in the collumns and returns an account object with these values
     */
    public static Account GetAccount(String accountNumber, String tableName) {
        if (!validateAccountNumber(accountNumber)) {
            throw new InvalidParameterException(String.format("%s is not a valid account number", accountNumber));
        }

        Account account = null;

        Connection currentConnection = getNewConnection();

        PreparedStatement statement;
        try {
            statement = currentConnection.prepareStatement("Select * from " + tableName + " where accountNumber = ?");

            statement.setString(1, accountNumber);
            statement.execute();

            ResultSet result = statement.getResultSet();

            //If we don't get an account back from a query we will return null.
            if (result.first()) {
                String fetchedAccountNumber = result.getString(1);
                double fetchedBalance = result.getDouble(2);
                double fetchedInterest = result.getDouble(3);
                account = new Account(fetchedAccountNumber, fetchedBalance, fetchedInterest);
            }



        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                currentConnection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return account;
    }
    /*
    Will update an account in the table with the new values in accountBalance and accountInterest
     */
    public static Account UpdateAccount(String accountNumber, double accountBalance, double accountInterest, String tableName){
        if (!validateAccountNumber(accountNumber)) {
            throw new InvalidParameterException(String.format("%s is not a valid account number", accountNumber));
        }

        Connection currentConnection = getNewConnection();

        PreparedStatement statement;
        try {
            statement = currentConnection.prepareStatement("UPDATE " + tableName + " " +
                    "SET balance = ?, interest = ? " +
                    "Where accountNumber = ?");
            statement.setDouble(1, accountBalance);
            statement.setDouble(2, accountInterest);
            statement.setString(3, accountNumber);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                currentConnection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return GetAccount(accountNumber, tableName);
    }
    /*
    Deletes an account from the database table with the give account number. Returns true if 1 or more row was
    affected(1 or more accounts deleted). Returns false if no accounts were deleted.
     */
    public static boolean DeleteAccount(String accountNumber, String tableName) {
        if (!validateAccountNumber(accountNumber)) {
            throw new InvalidParameterException(String.format("%s is not a valid account number", accountNumber));
        }

        Connection currentConnection = getNewConnection();

        PreparedStatement statement;
        int deleted = 0;
        try {
            statement = currentConnection.prepareStatement("DELETE FROM " + tableName + " " +
                    "WHERE accountNumber = ?");

            statement.setString(1, accountNumber);
            deleted = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                currentConnection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return deleted == 1;
    }
    /*
    Gets all accounts in the table and returns them as an ArrayList<Account>
     */
    public static ArrayList<Account> GetAllAccounts(String tableName) {
        Connection currentConnection = getNewConnection();

        PreparedStatement statement;
        ArrayList<Account> accountList = new ArrayList<Account>();

        try {
            statement = currentConnection.prepareStatement("SELECT * FROM " + tableName);
            statement.execute();

            ResultSet results = statement.getResultSet();

            if (results.first()) {
                do{
                    String fetchedAccountNumber = results.getString(1);
                    double fetchedBalance = results.getDouble(2);
                    double fetchedInterest = results.getDouble(3);
                    accountList.add(new Account(fetchedAccountNumber, fetchedBalance, fetchedInterest));
                } while (results.next());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                currentConnection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return accountList;
    }

    String getAccountNumber() {
        return accountNumber;
    }

    private void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    double getAccountBalance() {
        return accountBalance;
    }

    private void setAccountBalance(double accountBalance2) {
        this.accountBalance = accountBalance2;
    }

    double getAccountInterest() {
        return accountInterest;
    }

    private void setAccountInterest(double accountInterest) {
        this.accountInterest = accountInterest;
    }

    private static Connection getNewConnection() {
        DBConnection db = new DBConnection();

        return db.getDatabaseConnection();
    }

    /*
    Validates the account number by checking that you can cast it to integer and that its
    value is over 0 and length is 8
     */
    public static boolean validateAccountNumber(String accountNumber) {
        boolean validInput = false;
        int input = Integer.valueOf(accountNumber);
        if (accountNumber.length() == 8 && (input > 0)) {
            validInput = true;
        }
        return validInput;
    }

    public String toString() {

        return String.format("Account Number: %s, Account Balance: %f, Account Interest: %f", getAccountNumber(), getAccountBalance(), getAccountInterest());
    }

}
