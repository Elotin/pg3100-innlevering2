/*
 * Innlevering 2
 * Håkon Martin Eide.2.klasse programmering 2012
 * 22,10.2012
 */

package main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The purpose of this class is to provide a superclass of account update actions that defines some methods
 * that will be used to do account updates. Every account update can be one of the followin:
 * Add, Subtract or Replace/new. When you create an accountUpdater you pass in the action you
 * want and get an accountUpdater subclass that will have the executeUpdate method and perform
 * the update you specified.
 *
 * I decided to do it this way as it gives you simplest possible way in my mind to expand the functionality
 * of the account updating while keeping the interface api as small as possible, you only need two methods.
 */
public abstract class AccountUpdater {

    /*
   An enumerable describing the different actions you can do to a collumn/row.
    */
    protected enum Actions {
        ADD,
        SUBTRACT,
        REPLACE
    }
    /*
    Will parse the action string and return an accountUpdater string class that will update the value you selected in action
    when you execute update
     */
    public static AccountUpdater createAccountUpdater(String accountNumber, String action, double value) {
        action = action.toLowerCase();
        if (action.equals("b+"))
            return new AccountBalanceUpdater(accountNumber, Actions.ADD, value);
        else if (action.equals("b-"))
            return new AccountBalanceUpdater(accountNumber, Actions.SUBTRACT, value);
        else if (action.equals("b"))
            return new AccountBalanceUpdater(accountNumber, Actions.REPLACE, value);
        if (action.equals("i+"))
            return new AccountInterestUpdater(accountNumber, Actions.ADD, value);
        else if (action.equals("i-"))
            return new AccountInterestUpdater(accountNumber, Actions.SUBTRACT, value);
        else if (action.equals("i"))
            return new AccountInterestUpdater(accountNumber, Actions.REPLACE, value);
        else
            return new AccountNewEntryUpdater(accountNumber, Double.valueOf(action), value);
    }

    /*
    Executes the update to the given accountNumber in the database and table name.
     */
    public abstract Account executeUpdate(String tableName);

    /*
    Helper method to return a whole list of all the account update objects
    in a given table.
    Usefull if you want to enumerate through all the objects and update every one.
     */
    public static ArrayList<AccountUpdater> getAllAccountUpdates(String tableName) {
        Connection currentConnection = getNewConnection();

        PreparedStatement statement = null;
        ArrayList<AccountUpdater> accountList = new ArrayList<AccountUpdater>();

        try {
            statement = currentConnection.prepareStatement("SELECT * FROM " + tableName);
            statement.execute();

            ResultSet results = statement.getResultSet();

            if (results.first()) {
                do{
                    String fetchedAccountNumber = results.getString(1);
                    String fetchedAction = results.getString(2);
                    double fetchedValue = results.getDouble(3);
                    AccountUpdater accountUpdater = AccountUpdater.createAccountUpdater(fetchedAccountNumber, fetchedAction, fetchedValue);
                    if (accountUpdater != null) accountList.add(accountUpdater);
                } while (results.next());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                currentConnection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return accountList;
    }
    /*
    Forces the subclasses to implement toString.
    Helpful for reporting what the accountUpdate will actually do.
     */
    public abstract String toString();

    private static Connection getNewConnection() {
        DBConnection db = new DBConnection();

        return db.getDatabaseConnection();
    }
}
